package game.minedetect;

/**
 * Created by mm on 2017.12.08.
 */

public class Block {
    private boolean isMine = false;
    private int mineArround = 0;
    private int touched = 0;

    public boolean isMine() {
        return isMine;
    }

    public void setMine(boolean mine) {
        isMine = mine;
    }

    public int getMineArround() {
        return mineArround;
    }

    public void setMineArround(int mineArround) {
        this.mineArround = mineArround;
    }

    public int getTouched() {
        return touched;
    }

    public void setTouched(int touched) {
        this.touched = touched;
    }
}
