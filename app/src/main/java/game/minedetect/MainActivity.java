package game.minedetect;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnLongClickListener{
    private LinearLayout panel;
    private LinearLayout popup;
    private LinearLayout gameOver;
    private LinearLayout result;
    private  Button setting;
    private TextView score_1;
    private TextView score_2;
    private TextView score_3;
    private TextView score_4;
    private TextView score_5;
    private TextView score_6;
    private TextView score_7;
    private TextView timer;
    private TextView difficultyText;
    private TextView resultTime;

    private int[] buttonIds;
    private List<Integer> ranking = new ArrayList<>();
    private List<Block> blocks = new ArrayList<>();
    private List<Button> buttons = new ArrayList<>();

    private final int totalColumns = 15;
    private final int NORMAL = 1;
    private final int HARD = 50;
    private final int EXPERT = 60;
    private int difficulty = NORMAL;
    private int flagCount = 0;

    private MyHandler handler;
    private MyThread mThread;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        panel = (LinearLayout)findViewById(R.id.mine_panel);
        popup = (LinearLayout)findViewById(R.id.popup_layout);
        gameOver = (LinearLayout)findViewById(R.id.over_layout);
        result = (LinearLayout)findViewById(R.id.result_layout);
        score_1 = (TextView)findViewById(R.id.time_1);
        score_2 = (TextView)findViewById(R.id.time_2);
        score_3 = (TextView)findViewById(R.id.time_3);
        score_4 = (TextView)findViewById(R.id.time_4);
        score_5 = (TextView)findViewById(R.id.time_5);
        score_6 = (TextView)findViewById(R.id.time_6);
        score_7 = (TextView)findViewById(R.id.time_7);
        timer = (TextView)findViewById(R.id.timer);
        difficultyText = (TextView)findViewById(R.id.difficulty);
        resultTime = (TextView)findViewById(R.id.result_time);

        Button startBtn = (Button)findViewById(R.id.start_btn);
        startBtn.setOnClickListener(this);
        Button restartOver = (Button)findViewById(R.id.restart_over_btn);
        restartOver.setOnClickListener(this);
        Button restartResult = (Button)findViewById(R.id.restart_result_btn);
        restartResult.setOnClickListener(this);
        setting = (Button)findViewById(R.id.setting_btn);
        setting.setOnClickListener(this);

        pref = getSharedPreferences("leader_board",0);
        editor = pref.edit();

        for(int i = 0; i < 7 ; i++){
            int time = pref.getInt("rank_"+i, 0);
            if(time > 0) {
                ranking.add(time);
            }else{
                ranking.add(0);
            }
        }

        score_1.setText(convertScore(ranking.get(0)));
        score_2.setText(convertScore(ranking.get(1)));
        score_3.setText(convertScore(ranking.get(2)));
        score_4.setText(convertScore(ranking.get(3)));
        score_5.setText(convertScore(ranking.get(4)));
        score_6.setText(convertScore(ranking.get(5)));
        score_7.setText(convertScore(ranking.get(6)));

        mThread = new MyThread();
        handler = new MyHandler();

        buttonIds = generateIds();
        mThread.start();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

    }

    @Override
    protected void onResume() {
        super.onResume();
        generateBlocks(panel);
    }

    private int[] generateIds(){
        int[] ids = new int[totalColumns*totalColumns];
        for(int i = 0; i < (totalColumns*totalColumns); i++){
            ids[i] = View.generateViewId();
        }
        return ids;
    }

    private void generateBlocks(LinearLayout pan){
        for(int i = 0; i < totalColumns; i++) {
            LinearLayout row = new LinearLayout(this);
            row.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 85));
            row.setOrientation(LinearLayout.HORIZONTAL);
            row.setWeightSum(1080);
            for(int j = 0; j < totalColumns; j++){
                Button block = new Button(this);
                block.setId(buttonIds[(i*totalColumns) + j]);
                block.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 72));
                block.setBackgroundResource(R.drawable.block_background);
                block.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                block.setGravity(Gravity.CENTER);
                block.setPadding(0,0,0,0);
                block.setTextColor(Color.WHITE);
                block.setOnLongClickListener(this);
                block.setOnClickListener(this);
                block.setTag((i*totalColumns) + j);
                buttons.add(block);
                row.addView(block);
            }
            pan.addView(row);
        }
    }

    private void generateMines(int mineNumber){
        int count = 0;
        Random random = new Random();
        blocks.clear();
        for(Button b : buttons){
            b.setBackgroundResource(R.drawable.block_background);
            b.setText("");
            blocks.add(new Block());
        }
        while(count < mineNumber){
            int position = random.nextInt((totalColumns*totalColumns));
            if(!blocks.get(position).isMine()){
                count++;
                blocks.get(position).setMine(true);
            }
        }

        for(int i = 0; i < blocks.size(); i++){
            if(!blocks.get(i).isMine()){
                int number = checkBlock(i);
                if(number > 0) {
                    blocks.get(i).setMineArround(number);
                }else{
                    blocks.get(i).setMineArround(0);
                }
            }
        }
    }

    private void reveal(int position){
        buttons.get(position).setBackgroundResource(R.drawable.block_pressed_background);
        if(blocks.get(position).getMineArround() > 0) {
            blocks.get(position).setTouched(-1);
            buttons.get(position).setText(String.format("%d", blocks.get(position).getMineArround()));
        }else{
            if(blocks.get(position).getTouched() > -2) {
                blocks.get(position).setTouched(-2);
                check9ways(position);
            }
        }
    }

    private void forceReveal(int position){
        if(blocks.get(position).getTouched() == 0) {
            if (blocks.get(position).isMine()) {
                revealAll();
                mThread.timerStop();
                gameOver.setVisibility(View.VISIBLE);
            } else {
                reveal(position);
            }
        }
    }

    private void revealAll(){
        for(int i = 0; i < blocks.size(); i++){
            if(blocks.get(i).isMine()){
                buttons.get(i).setBackgroundResource(R.drawable.mine_background);
                buttons.get(i).setText("");
            }else{
                buttons.get(i).setBackgroundResource(R.drawable.block_pressed_background);
                if(blocks.get(i).getMineArround() > 0) {
                    buttons.get(i).setText(String.format("%d", blocks.get(i).getMineArround()));
                }
            }
        }
    }

    private void reveal9Ways(int position){
        int checkedFlagCount = 0;

        if((position - totalColumns) < 0){ //가장 윗줄
            if(position%totalColumns == 0){ //가장 왼쪽
                if(blocks.get(position +1).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position + totalColumns).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position + totalColumns +1).getTouched() > 0 ) checkedFlagCount++;
            }else if(position%totalColumns == (totalColumns - 1)){ //가장 오른쪽
                if(blocks.get(position -1).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position + totalColumns).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position + totalColumns -1).getTouched() > 0 ) checkedFlagCount++;
            }else{
                if(blocks.get(position +1).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position -1).getTouched() > 0 ) checkedFlagCount++;
                for(int i = -1; i <= 1; i++) {
                    if(blocks.get(position + totalColumns -i).getTouched() > 0 ) checkedFlagCount++;
                }
            }
        }else if((position + totalColumns) > (totalColumns*totalColumns)-1){ //가장 아랫줄
            if(position%totalColumns == 0){ //가장 왼쪽
                if(blocks.get(position +1).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position - totalColumns).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position - totalColumns +1).getTouched() > 0 ) checkedFlagCount++;
            }else if(position%totalColumns == (totalColumns - 1)){ //가장 오른쪽
                if(blocks.get(position -1).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position - totalColumns).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position - totalColumns -1).getTouched() > 0 ) checkedFlagCount++;
            }else{
                if(blocks.get(position - 1).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position + 1).getTouched() > 0 ) checkedFlagCount++;
                for(int i = -1; i <= 1; i++) {
                    if(blocks.get(position - totalColumns -i).getTouched() > 0 ) checkedFlagCount++;
                }
            }
        }else{
            if(position%totalColumns == 0){ //가장 왼쪽
                if(blocks.get(position - totalColumns).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position - totalColumns + 1).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position + 1).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position + totalColumns).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position + totalColumns + 1).getTouched() > 0 ) checkedFlagCount++;
            }else if(position%totalColumns == (totalColumns - 1)){ //가장 오른쪽
                if(blocks.get(position - totalColumns).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position - totalColumns - 1).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position - 1).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position + totalColumns).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position + totalColumns - 1).getTouched() > 0 ) checkedFlagCount++;
            }else{
                for(int i = -1; i <= 1; i++) {
                    if(blocks.get(position - totalColumns - i).getTouched() > 0 ) checkedFlagCount++;
                }
                if(blocks.get(position + 1).getTouched() > 0 ) checkedFlagCount++;
                if(blocks.get(position - 1).getTouched() > 0 ) checkedFlagCount++;
                for(int i = -1; i <= 1; i++) {
                    if(blocks.get(position + totalColumns - i).getTouched() > 0 ) checkedFlagCount++;
                }
            }
        }

        if(blocks.get(position).getMineArround() == checkedFlagCount){
            if((position - totalColumns) < 0){ //가장 윗줄
                if(position%totalColumns == 0){ //가장 왼쪽
                    forceReveal(position +1);
                    forceReveal(position +totalColumns);
                    forceReveal(position + totalColumns + 1);
                }else if(position%totalColumns == (totalColumns - 1)){ //가장 오른쪽
                    forceReveal(position - 1);
                    forceReveal(position + totalColumns);
                    forceReveal(position + totalColumns - 1);
                }else{
                    forceReveal(position - 1);
                    forceReveal(position + 1);
                    for(int i = -1; i <= 1; i++) {
                        forceReveal(position + totalColumns - i);
                    }
                }
            }else if((position + totalColumns) > (totalColumns*totalColumns)-1){ //가장 아랫줄
                if(position%totalColumns == 0){ //가장 왼쪽
                    forceReveal(position + 1);
                    forceReveal(position - totalColumns);
                    forceReveal(position - totalColumns + 1);
                }else if(position%totalColumns == (totalColumns - 1)){ //가장 오른쪽
                    forceReveal(position - 1);
                    forceReveal(position - totalColumns);
                    forceReveal(position - totalColumns - 1);
                }else{
                    forceReveal(position - 1);
                    forceReveal(position + 1);
                    for(int i = -1; i <= 1; i++) {
                        forceReveal(position - totalColumns - i);
                    }
                }
            }else{
                if(position%totalColumns == 0){ //가장 왼쪽
                    forceReveal(position - totalColumns + 1);
                    forceReveal(position - totalColumns);
                    forceReveal(position + 1);
                    forceReveal(position + totalColumns + 1);
                    forceReveal(position + totalColumns);
                }else if(position%totalColumns == (totalColumns - 1)){ //가장 오른쪽
                    forceReveal(position - totalColumns - 1);
                    forceReveal(position - totalColumns);
                    forceReveal(position - 1);
                    forceReveal(position + totalColumns - 1);
                    forceReveal(position + totalColumns);
                }else{
                    for(int i = -1; i <= 1; i++) {
                        forceReveal(position - totalColumns - i);
                    }
                    forceReveal(position + 1);
                    forceReveal(position - 1);
                    for(int i = -1; i <= 1; i++) {
                        forceReveal(position + totalColumns - i);
                    }
                }
            }
        }
    }

    private void check9ways(int position){
        if((position - totalColumns) < 0){ //가장 윗줄
            if(position%totalColumns == 0){ //가장 왼쪽
                reveal(position +1);
                reveal(position +totalColumns);
                reveal(position + totalColumns + 1);
            }else if(position%totalColumns == (totalColumns - 1)){ //가장 오른쪽
                reveal(position - 1);
                reveal(position + totalColumns);
                reveal(position + totalColumns - 1);
            }else{
                reveal(position - 1);
                reveal(position + 1);
                for(int i = -1; i <= 1; i++) {
                    reveal(position + totalColumns - i);
                }
            }
        }else if((position + totalColumns) > (totalColumns*totalColumns)-1){ //가장 아랫줄
            if(position%totalColumns == 0){ //가장 왼쪽
                reveal(position + 1);
                reveal(position - totalColumns);
                reveal(position - totalColumns + 1);
            }else if(position%totalColumns == (totalColumns - 1)){ //가장 오른쪽
                reveal(position - 1);
                reveal(position - totalColumns);
                reveal(position - totalColumns - 1);
            }else{
                reveal(position - 1);
                reveal(position + 1);
                for(int i = -1; i <= 1; i++) {
                    reveal(position - totalColumns - i);
                }
            }
        }else{
            if(position%totalColumns == 0){ //가장 왼쪽
                reveal(position - totalColumns + 1);
                reveal(position - totalColumns);
                reveal(position + 1);
                reveal(position + totalColumns + 1);
                reveal(position + totalColumns);
            }else if(position%totalColumns == (totalColumns - 1)){ //가장 오른쪽
                reveal(position - totalColumns - 1);
                reveal(position - totalColumns);
                reveal(position - 1);
                reveal(position + totalColumns - 1);
                reveal(position + totalColumns);
            }else{
                for(int i = -1; i <= 1; i++) {
                    reveal(position - totalColumns - i);
                }
                reveal(position + 1);
                reveal(position - 1);
                for(int i = -1; i <= 1; i++) {
                    reveal(position + totalColumns - i);
                }
            }
        }
    }

    private int checkBlock(int position){
        int count = 0;

        if((position - totalColumns) < 0){ //가장 윗줄
            if(position%totalColumns == 0){ //가장 왼쪽
                if(blocks.get(position + 1).isMine()) count++;
                if(blocks.get(position + totalColumns).isMine()) count++;
                if(blocks.get(position + totalColumns + 1).isMine()) count++;
            }else if(position%totalColumns == 14){ //가장 오른쪽
                if(blocks.get(position - 1).isMine()) count++;
                if(blocks.get(position + totalColumns).isMine()) count++;
                if(blocks.get(position + totalColumns - 1).isMine()) count++;
            }else{
                if(blocks.get(position - 1).isMine()) count++;
                if(blocks.get(position + 1).isMine()) count++;
                for(int i = -1; i <= 1; i++) {
                    if (blocks.get(position + totalColumns - i).isMine()) {
                        count++;
                    }
                }
            }
        }else if((position + totalColumns) > (totalColumns*totalColumns) - 1){ //가장 아랫줄
            if(position%totalColumns == 0){ //가장 왼쪽
                if(blocks.get(position + 1).isMine()) count++;
                if(blocks.get(position - totalColumns).isMine()) count++;
                if(blocks.get(position - totalColumns + 1).isMine()) count++;
            }else if(position%totalColumns == 14){ //가장 오른쪽
                if(blocks.get(position - 1).isMine()) count++;
                if(blocks.get(position - totalColumns).isMine()) count++;
                if(blocks.get(position - totalColumns - 1).isMine()) count++;
            }else{
                if(blocks.get(position - 1).isMine()) count++;
                if(blocks.get(position + 1).isMine()) count++;
                for(int i = -1; i <= 1; i++) {
                    if (blocks.get(position - totalColumns - i).isMine()) {
                        count++;
                    }
                }
            }
        }else{
            if(position%totalColumns == 0){ //가장 왼쪽
                if(blocks.get(position - totalColumns + 1).isMine()) count++;
                if(blocks.get(position - totalColumns).isMine()) count++;
                if(blocks.get(position + 1).isMine()) count++;
                if(blocks.get(position + totalColumns + 1).isMine()) count++;
                if(blocks.get(position + totalColumns).isMine()) count++;
            }else if(position%totalColumns == 14){ //가장 오른쪽
                if(blocks.get(position - totalColumns - 1).isMine()) count++;
                if(blocks.get(position - totalColumns).isMine()) count++;
                if(blocks.get(position - 1).isMine()) count++;
                if(blocks.get(position + totalColumns - 1).isMine()) count++;
                if(blocks.get(position + totalColumns).isMine()) count++;
            }else{
                for(int i = -1; i <= 1; i++) {
                    if (blocks.get(position - totalColumns - i).isMine()) {
                        count++;
                    }
                }
                if(blocks.get(position - 1).isMine()) count++;
                if(blocks.get(position + 1).isMine()) count++;
                for(int i = -1; i <= 1; i++) {
                    if (blocks.get(position + totalColumns - i).isMine()) {
                        count++;
                    }
                }
            }
        }

        return count;
    }

    private boolean checkFlag(){
        int mine = difficulty;

        for(Block b : blocks){
            if(b.getTouched() == 1){
                if(b.isMine()){
                    mine--;
                }
            }
        }

        if(mine == 0){
            return true;
        }else{
            return false;
        }
    }

    private void finishGame(){
        mThread.timerStop();
        gameOver.setVisibility(View.VISIBLE);
        int index = 0;
        for(int rank : ranking){
            if(rank <= mThread.getSec()){
                break;
            }
            index++;
        }

        ranking.add(index, mThread.getSec());
        for(int i = 0; i < 7 ; i++) {
            editor.putInt("rank_"+i, ranking.get(i));
        }
        editor.commit();
    }

    private String convertScore(int time){
        String res = "";

        if(time > 0) {
            int min = time/60;
            int sec = time%60;
            res = String.format("%02d분 %02d초", min, sec);
        }else{
            res = "기록 없음";
        }
        return res;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.start_btn:
                mThread.timerStart();
                timer.setText("0분 0초");
                switch (difficulty){
                    case NORMAL:
                        difficultyText.setText("NORMAL");
                        break;
                    case HARD:
                        difficultyText.setText("HARD");
                        break;
                    case EXPERT:
                        difficultyText.setText("EXPERT");
                        break;
                }
                generateMines(difficulty);
                popup.setVisibility(View.GONE);
                break;
            case R.id.restart_over_btn:
                gameOver.setVisibility(View.GONE);
                popup.setVisibility(View.VISIBLE);
                break;
            case R.id.restart_result_btn:
                gameOver.setVisibility(View.GONE);
                popup.setVisibility(View.VISIBLE);
                break;
            case R.id.setting_btn:
                PopupMenu pMenu = new PopupMenu(this, setting);
                pMenu.getMenuInflater().inflate(R.menu.main_menu, pMenu.getMenu());
                pMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.dif_normal:
                                difficulty = NORMAL;
                                difficultyText.setText("NORMAL");
                                timer.setText("0분 0초");
                                generateMines(NORMAL);
                                break;
                            case R.id.dif_hard:
                                difficulty = HARD;
                                difficultyText.setText("HARD");
                                timer.setText("0분 0초");
                                generateMines(HARD);
                                break;
                            case R.id.dif_expert:
                                difficulty = EXPERT;
                                difficultyText.setText("EXPERT");
                                timer.setText("0분 0초");
                                generateMines(EXPERT);
                                break;
                        }
                        return true;
                    }
                });
                pMenu.show();
                break;
            default:
                for(int id : buttonIds){
                    if(v.getId() == id){
                        int position = (int)v.getTag();
                        switch (blocks.get(position).getTouched()){
                            case -1:
                                reveal9Ways(position);
                                break;
                            case 0:
                                buttons.get(position).setBackgroundResource(R.drawable.check_flag);
                                blocks.get(position).setTouched(1);
                                flagCount++;
                                if(flagCount == difficulty){
                                    if(checkFlag()){
                                        finishGame();
                                    }
                                }
                                break;
                            case 1:
                                buttons.get(position).setBackgroundResource(R.drawable.check_question);
                                flagCount--;
                                blocks.get(position).setTouched(2);
                                break;
                            case 2:
                                buttons.get(position).setBackgroundResource(R.drawable.block_background);
                                blocks.get(position).setTouched(0);
                                break;
                        }
                    }
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        for(int id : buttonIds){
            if(v.getId() == id){
                int position = (int)v.getTag();
                forceReveal(position);
            }
        }
        return true;
    }

    private class MyThread extends Thread{
        private int sec = 0;
        private boolean running = false;

        @Override
        public void run() {
            super.run();
            try {
                while(true) {
                    if(running) {
                        Message msg = new Message();
                        msg.arg1 = sec++;
                        handler.sendMessage(msg);
                    }
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public int getSec(){
            return sec;
        }

        public void timerStop(){
            running = false;
        }

        public void timerStart(){
            running = true;
            sec = 0;
        }
    }

    private class MyHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int sec = msg.arg1;
            int min = sec / 60;
            sec = sec%60;
            timer.setText(String.format("%d분 %d초", min, sec));
        }
    }

    private void print(String s){
        System.out.println(s);
    }
}
